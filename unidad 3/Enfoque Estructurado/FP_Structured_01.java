import java.util.List;
public class FP_Structured_01 {
    public static void main(String[] args) {
        
        List<Integer> numbers=List.of(12,9,13,4,6,2,4,12,15);   
    FP_structure_01.printAllNumbersInListStructure(numbers);
        System.out.println("");
    FP_structure_01.printEvenNumbersInListStructure(numbers);
        System.out.println("-");
        printAllNumbersInListStructure(List.of(12,9,13,4,6,2,4,12,15));
        System.out.println("");
        printEvenNumbersInListStructure(List.of(12,9,13,4,6,2,4,12,15));
        System.out.println("");
    }
    
    private static  void printAllNumbersInListStructure(List<Integer>numbers){
        for (int number: numbers) {
            System.out.println(number+", ");
        }
        System.out.println("");
    }
    
    private static void printEvenNumbersInListStructure(List<Integer> numbers){
        for(int number:numbers){
            if (number % 2==0){
                System.out.println(number+", ");
            }
            System.out.println("");    
        }
    }
    
}
