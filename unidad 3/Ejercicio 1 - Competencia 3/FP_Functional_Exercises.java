import java.util.List;
import java.util.function.Consumer;

class FP_Functional_Exercises{
	public static void main(String[] args) {
		List<Integer> numbers = List.of(12,9,13,4,6,2,4,12,15);
		List<String> courses  = List.of("Spring","Spring Boot","API","Microservices","AWS","PCF","Azure","Docker","Kumbernetes","lalo");
		System.out.println("Ejercicio 1 ");
		numbers.stream().filter(numeros -> numeros % 2 !=0)
		.forEach(numeros -> System.out.println(" "+ numeros));

		System.out.println("Ejercicio 2 ");
 		courses.stream().forEach(le-> System.out.println(le+", "));
 		System.out.println("\n ");

		System.out.println("Ejercicio 3 "); 
			courses.stream().filter(palabra -> palabra.contains("Spring")).forEach(FP_Functional_Exercises::print);
		System.out.println("\n ");

		System.out.println("Ejercicio 5 "); 
		 FP_Functional_Exercises.ejer5(numbers);
         System.out.println("");
         System.out.println("Ejercicio 6 "); 
         FP_Functional_Exercises.ejer6(courses);
         System.out.println("");




	}

		private static void print(String palabra){
			 System.out.print(palabra + ", ");
		}

		private static void print(char palabra){
			 System.out.print(palabra + ", ");
}

     public static void ejer5(List<Integer>numbers){
         for (int i = 0; i < numbers.size(); i++){
             int a = (int)numbers.get(i);
             if (a % 2 != 0){
                 int res= (int)Math.pow(a, 3);
                 System.out.println(res);
             }
         }
         System.out.println("");
     }
     public static void ejer6(List<String>courses){
        courses.forEach(new Consumer<String>(){
            public void accept(final String pala){
                System.out.print(pala);
                System.out.print(" contien: ");
                System.out.print(pala.length());
                System.out.println(" letras");

            }
        });

     }


}
